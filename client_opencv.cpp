#include "opencv2/opencv.hpp"
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>

using namespace cv;

int main(int argc, char** argv)
{
    int         socket_fd;
    char*       serverIP;
    int         serverPort;

    if (argc < 3) {
           printf("Usage: cv_video_cli <serverIP> <serverPort> \n");
    }

    serverIP   = argv[1];
    serverPort = atoi(argv[2]);

    struct  sockaddr_in serverAddr;
    socklen_t           addrLen = sizeof(struct sockaddr_in);

    if ((socket_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket() failed\n");
    }

    serverAddr.sin_family = PF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(serverIP);
    serverAddr.sin_port = htons(serverPort);

    if (connect(socket_fd, (sockaddr*)&serverAddr, addrLen) < 0) {
        perror("connect() failed!\n");
    }

    Mat img;
    img = Mat::zeros(1080 , 1920, CV_8UC3);
    int imgSize = img.total() * img.elemSize();
    uchar *iptr = img.data;
    int bytes = 0;
    char send_buf[2];

    if ( ! img.isContinuous() ) { 
        img = img.clone();
    }
        
    printf("Image Size: %d\n",imgSize);

    namedWindow("CV Video Client", 1);

    while (1) {
        if ((bytes = recv(socket_fd, iptr, imgSize , MSG_WAITALL)) == -1) {
            printf("recv failed, received bytes = %d\n",bytes);
        }
        
        cv::imshow("CV Video Client", img);

        if (cv::waitKey(1) == 27) 
        {
            break;
        }
    }
    send_buf[0]=1;
    send(socket_fd, send_buf, sizeof(send_buf), 0);

    close(socket_fd);

    return 0;
}
