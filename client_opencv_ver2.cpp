#include "opencv2/opencv.hpp"
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

typedef unsigned short u16;
void *recv_data(void *);

enum{
    CAM_NUM       = 2,
    IMAGE_SIZE    = 1920*1080*3,
    RING_BUF_SIZE = 5,
    RING_BUF_END  = RING_BUF_SIZE,
    RING_BUF_START =0
};

class Ring_buffer
{
    public:
    std::vector<cv::Mat> ring_buf;                          // ring buffer
    int ring_buf_pointer;                                   // buffer pointer

    public:
    Ring_buffer():ring_buf_pointer(0)
    {
        ring_buf.reserve(RING_BUF_SIZE);
        for(int i=0;i<RING_BUF_SIZE;i++)
        {
            ring_buf[i].create(1080,1920,CV_8UC3);
        }
    }
    void Ring_Buffer_Push(cv::Mat);
    uchar* Ring_Buffer_Pop(void);
};

void Ring_buffer::Ring_Buffer_Push(cv::Mat mat)
{
    mat.copyTo(ring_buf[ring_buf_pointer]);         //ring buffer에 복사
    ring_buf_pointer++;                             //pointer 이동

    // printf("push!\n");
    
    if (ring_buf_pointer == RING_BUF_END) {         //버퍼 끝에 도달 시 pointer 0으로 초기화
        ring_buf_pointer = RING_BUF_START;
    }
}

uchar* Ring_buffer::Ring_Buffer_Pop(void)
{
    uchar *ret = 0;
    // printf("%d\n",ring_buf_pointer);              //debug용 현재 포인터 출력
    int previous=ring_buf_pointer-1;                 //이전 값은 현재 위치-1(한 칸 전의 데이터)

    if (ring_buf_pointer >= 1) {                     //링버퍼 안에 데이터 있을 시
        ret = ring_buf[previous].data;               //ret에 링버퍼에 저장되어있는 최신 데이터의 포인터 저장
        // printf("pop! \n");
    }
    else if(ring_buf_pointer == RING_BUF_START)      //끝에 도달하여 초기 칸으로 포인터가 돌아올 경우              
    {
        if(ring_buf[RING_BUF_END-1].at<cv::Vec3b>(0,0)[0] != 0)            //초기에 push없이 pop하는거 방지용
        {
            ret = ring_buf[RING_BUF_END-1].data;           //ret에 링버퍼의 마지막에 저장되어있는 데이터의 포인터 저장
            // printf("last pop! \n");
        }
        else
        {
            // printf("first\n");
            ret=NULL;
        }
        
    }
    else                                             //데이터 없을경우 NULL 반환
    {
        // printf("no data\n");
        ret=NULL;
    }
    return ret;
}
 
Ring_buffer Ring;

int main(int argc, char** argv)
{
    int         socket_fd;
    char*       serverIP;
    int         serverPort;
    int *thread_data = &socket_fd;
    pthread_t thread;
	int thr_id;
    cv::Mat frame2(1080,1920,CV_8UC3);

    uchar *ptr;

    if (argc < 3) {
           printf("Usage: cv_video_cli <serverIP> <serverPort> \n");
    }
    /*debug*/
    // char addr[10];
    // strcpy(addr,"127.0.0.1");
    // serverIP   = addr;//argv[1]
    // serverPort = 4097;//atoi(argv[2])

    serverIP   = argv[1];
    serverPort = atoi(argv[2]);

    struct  sockaddr_in serverAddr;
    socklen_t           addrLen = sizeof(struct sockaddr_in);

    if ((socket_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket() failed\n");
    }

    serverAddr.sin_family = PF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(serverIP);
    serverAddr.sin_port = htons(serverPort);

    if (connect(socket_fd, (sockaddr*)&serverAddr, addrLen) < 0) {
        perror("connect() failed!\n");
    }

    thr_id = pthread_create(&thread, NULL, recv_data,thread_data);  
                                                    //thread 생성 후 cap 함수 돌림
    if(thr_id < 0)                                  //thread 생성 실패 시
    {
        perror("thread create error");
        exit(EXIT_FAILURE);
    }

    while (1) {
        ptr=Ring.Ring_Buffer_Pop();
        
          if(ptr!=NULL)                               //링버퍼에 데이터가 존재 할 경우에만 동작
        {
            memcpy(frame2.data,ptr,IMAGE_SIZE);     //프레임에 이미지 복사
            cv::imshow("CV Video Client", frame2);
        }
        else
        {
            //do nothing
        }

        if (cv::waitKey(1) == 27) 
        {
            break;
        }
        usleep(30000);
    }

    char send_buf[2];                                   //서버에 종료를 알림.
    send_buf[0]=1;
    send(socket_fd, send_buf, sizeof(send_buf), 0);

    close(thread);
    close(socket_fd);

    return 0;
}

void *recv_data(void *data)                             //서버로부터 받아서 링버퍼에 저장하는 thread
{
    int socket_fd=*(int*)data;
    cv::Mat img;
    img = cv::Mat::zeros(1080 , 1920, CV_8UC3);
    int imgSize = img.total() * img.elemSize();

    printf("Image Size: %d\n",imgSize);
    int bytes = 0;

   while (1) {
        if ((bytes = recv(socket_fd, img.data, imgSize , MSG_WAITALL)) == -1) {
            printf("recv failed, received bytes = %d\n",bytes);
        }

        /*debug*/
        // cv::imshow("CV Video Client", img);

        // if (cv::waitKey(1) == 27) 
        // {
        //     break;
        // }

        Ring.Ring_Buffer_Push(img);
   }
}
