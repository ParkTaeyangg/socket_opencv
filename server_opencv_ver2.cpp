//server is same as server_opencv.cpp
#include "opencv2/opencv.hpp"
#include <stdio.h>
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h> 
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

using namespace cv;

void *send_data(void *);
void *recv_data(void *);

sem_t semaphore;
bool running = false;

int main(int argc, char** argv)
{   
    sem_init(&semaphore, 0, 1);

    int localSocket,
        remoteSocket,
        port = 4097;                               
    struct  sockaddr_in localAddr,
                        remoteAddr;
    int addrLen = sizeof(struct sockaddr_in);

    pthread_t send_thread;
    pthread_t recv_thread;
       
    if ( (argc > 1) && (strcmp(argv[1],"-h") == 0) ) {
        perror("usage: ./tcpser [port] [capture device]\nport: socket port (4097 default)\ncapture device : (2 default)\n\n");

        exit(1);
    }

    if (argc == 2) port = atoi(argv[1]);

    localSocket = socket(AF_INET , SOCK_STREAM , 0);
    if (localSocket == -1){
        perror("socket() call failed!!");
    }    

    localAddr.sin_family = AF_INET;
    localAddr.sin_addr.s_addr = INADDR_ANY;
    localAddr.sin_port = htons( port );

    if( bind(localSocket,(struct sockaddr *)&localAddr , sizeof(localAddr)) < 0) {
        perror("Can't bind() socket");
        exit(1);
    }
    
    // Listening
    listen(localSocket , 3);
    
    printf("Waiting for connections...\nServer Port: %d\n",port); 

    //accept connection from an incoming client
    remoteSocket = accept(localSocket, (struct sockaddr *)&remoteAddr, (socklen_t*)&addrLen);  
    
    if (remoteSocket < 0) {
        perror("accept failed!");
        exit(1);
    } 
    
    printf("Connection accepted\n");
    pthread_create(&send_thread, NULL, send_data, &remoteSocket);
    pthread_create(&recv_thread, NULL, recv_data, &remoteSocket);

    pthread_join(send_thread, NULL);
    pthread_join(recv_thread, NULL);

    close(remoteSocket);
    close(send_thread);
    close(recv_thread);
    printf("Bye Bye~\n");
    return 0;
}

void *send_data(void *ptr){     //현재 카메라로 이미지를 캡쳐한 뒤, calibration 후 전송
    int socket = *(int *)ptr;
    cv::Mat cameraMatrix= cv::Mat::zeros(3, 3, CV_64FC1); //eyes로도 초기화 가능 - matlab style
    cv::Mat distCoeffs = cv::Mat::zeros(1, 5, CV_64FC1);
    Mat img,temp;

    cameraMatrix=(Mat1d(3, 3) <<2119.812805052573, 0, 961.24513264214, 0, 2133.458529664271, 541.960848740249, 0, 0, 1); 
    distCoeffs=(Mat1d(1, 5) <<-0.4223737459578336, -0.0713131737267596, -0.0008729676729526053, 0.003638952889561101, 0.732611653877832); 

    VideoCapture cap(2);
    if (!cap.isOpened()) 
    {
        perror("connect camera!\n");
    } 

    Mat map1,map2;
    initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), cameraMatrix, Size(1920,1080), CV_32FC1, map1, map2);

    int height = cap.get(CAP_PROP_FRAME_HEIGHT);
    int width = cap.get(CAP_PROP_FRAME_WIDTH);

    printf("height: %d\n",height);
    printf("width: %d\n",width);

    img = Mat::zeros(height, width, CV_8UC3);
    temp = Mat::zeros(height, width, CV_8UC3);

    int imgSize = img.total() * img.elemSize();
    int bytes = 0;

    // make img continuos
    if(!img.isContinuous()){ 
        img = img.clone();
    }
    printf("Image Size: %d\n",imgSize);

    bool status = true;

    while(status){
        cap >> img;
        remap(img, temp, map1, map2, INTER_LINEAR);

        if ((bytes = send(socket, temp.data, imgSize, 0)) < 0){
            printf("bytes = %d\n",bytes);
            break;
        }

        sem_wait(&semaphore);
        if(running) status = false;
        sem_post(&semaphore);
    }
    printf("send quitting..\n");
}

void *recv_data(void *ptr){                     //1을 전송 받으면 종료시키는 thread 
    int socket = *(int *)ptr;
    char buffer[2];
    int bytes;
    while(1){
        bytes = recv(socket, buffer, sizeof(buffer), MSG_WAITALL);

        if (bytes < 0){
            printf("error receiving from client\n");
        }
        else if (bytes > 0){
            if (buffer[0]){
                sem_wait(&semaphore);
                running = true;
                sem_post(&semaphore);
                break;
            }
        }
    }
    printf("recv quitting...\n");
}
